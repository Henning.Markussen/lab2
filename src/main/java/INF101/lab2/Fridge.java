package INF101.lab2;

import javax.sql.rowset.FilteredRowSet;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    private int maxCapacity = 20;
    List<FridgeItem> stock = new ArrayList<FridgeItem>();

    @Override
    public int nItemsInFridge() {
        return stock.size();
    }

    @Override
    public int totalSize() {
        return maxCapacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (totalSize()==nItemsInFridge()){
            System.out.println("Fridge is full");
            return false;
        }
        stock.add(item);

        return true;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!stock.contains(item)){
            throw new NoSuchElementException();
        }
        stock.contains(item);
        stock.remove(item);

    }

    @Override
    public void emptyFridge() {
        while(nItemsInFridge()>0){
            FridgeItem head = stock.get(0);
            takeOut(head);
        }
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expired = new ArrayList<FridgeItem>();
        for (FridgeItem i: stock){
            if (i.hasExpired()){
                expired.add(i);
            }
        }
        for (FridgeItem j: expired){
            takeOut(j);
        }
        return expired;
    }
}
